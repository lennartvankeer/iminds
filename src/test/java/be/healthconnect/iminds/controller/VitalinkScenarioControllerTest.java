package be.healthconnect.iminds.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.concurrent.Callable;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.easymock.EasyMock.expect;

import be.ehealth.technicalconnector.exception.SessionManagementException;
import be.ehealth.technicalconnector.exception.TechnicalConnectorException;
import be.healthconnect.iminds.authenticationConfig.AuthenticationConfig;
import be.healthconnect.iminds.authenticationConfig.SamlAttribute;
import be.healthconnect.iminds.authenticationConfig.SamlDesignator;
import be.healthconnect.iminds.authenticationConfig.Type;
import be.healthconnect.iminds.concurrent.CallResponse;
import be.healthconnect.iminds.controller.impl.VitalinkScenarioControllerImpl;
import be.healthconnect.iminds.service.LoginService;
import be.healthconnect.iminds.util.I18NUtility;
import be.healthconnect.iminds.view.VitalinkScenarioView;
import be.healthconnect.iminds.vitalinkScenario.Dataentry;
import be.smals.safe.connector.VitalinkService;
import be.smals.safe.connector.domain.DataEntry;
import be.smals.safe.connector.domain.Status;
import be.smals.safe.connector.domain.protocol.RemoveDataEntryRequest;
import be.smals.safe.connector.domain.protocol.RemoveDataEntryResponse;
import be.smals.safe.connector.domain.protocol.StoreDataEntriesRequest;
import be.smals.safe.connector.domain.protocol.StoreDataEntriesResponse;

public class VitalinkScenarioControllerTest {

	private VitalinkScenarioControllerImpl controller;
	private VitalinkScenarioView view;
	private LoginService loginService;
	private VitalinkService vitalinkService;
	
	@Before
	public void setUp(){
		view = EasyMock.createMock(VitalinkScenarioView.class);
		loginService = EasyMock.createMock(LoginService.class);
		vitalinkService = EasyMock.createMock(VitalinkService.class);
		controller = new VitalinkScenarioControllerImpl(view, loginService, vitalinkService){
			/**
			 * {@inheritDoc}
			 * 
			 * Cannot use JavaFX Task.
			 */
			@Override
			protected <T> void doAsync(Callable<T> task, CallResponse<T> onSuccess, CallResponse<Throwable> onFailure) {
				try {
					final T result = task.call();
					if (onSuccess != null) {
						onSuccess.response(result);
					}
				} catch (Exception e) {
					if (onFailure != null) {
						onFailure.response(e);
					}
				}
			}
		};
		
	}
	
	@Test
	public void testRunTest() throws SessionManagementException, UnrecoverableKeyException, TechnicalConnectorException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
		URL url = VitalinkScenarioControllerTest.class.getResource("/vitalinkScriptExample.xml");
		File file = new File(url.getFile());
		
		Capture<AuthenticationConfig> configCapture = Capture.newInstance();
		Capture<StoreDataEntriesRequest> dataEntryRequestCapture = Capture.newInstance();
		Capture<RemoveDataEntryRequest> removeRequestCapture = Capture.newInstance();
		
		//expects
		view.inform(I18NUtility.getMessage("vitalinkscenario.started"));
		view.inform(I18NUtility.getMessage("vitalinkscenario.scenarioparsed"));
		loginService.doLogin(EasyMock.capture(configCapture));
		view.inform(I18NUtility.getMessage("vitalinkscenario.loginSuccess"));	
		
		StoreDataEntriesResponse storeResponse = new StoreDataEntriesResponse();
		storeResponse.setStatus(new Status(200, "status1"));
		expect(vitalinkService.storeDataEntries(EasyMock.capture(dataEntryRequestCapture))).andReturn(storeResponse);
		view.inform("status1");
		
		RemoveDataEntryResponse removeResponse = new RemoveDataEntryResponse();
		removeResponse.setStatus(new Status(200, "status2"));
		expect(vitalinkService.removeDataEntry(EasyMock.capture(removeRequestCapture))).andReturn(removeResponse);
		view.inform("status2");
		
		view.inform(I18NUtility.getMessage("vitalinkscenario.stopped.success"));
		
		
		EasyMock.replay(view, loginService, vitalinkService);
		controller.runTest(file);
		EasyMock.verify(view, loginService, vitalinkService);
		
		AuthenticationConfig config = configCapture.getValue();
		assertEquals("81062205558", config.getSsin());
		assertEquals("password2012", config.getMainCertificatePassword());
		assertEquals(Type.FALL_BACK, config.getType());
		assertEquals("./src/test/resources/P12/SSIN=81062205558 20150706-131321.acc-p12", config.getMainCertificateLocation());
		
		SamlAttribute saml0 = config.getSamlAttribute().get(0);
		assertEquals("urn:be:fgov:identification-namespace", saml0.getNamespace());
		assertEquals("urn:be:fgov:ehealth:1.0:certificateholder:person:ssin", saml0.getKey());
		assertEquals("81062205558", saml0.getValue());
		
		SamlAttribute saml1 = config.getSamlAttribute().get(1);
		assertEquals("urn:be:fgov:identification-namespace", saml1.getNamespace());
		assertEquals("urn:be:fgov:person:ssin", saml1.getKey());
		assertEquals("81062205558", saml1.getValue());
		
		SamlDesignator des0 = config.getSamlDesignator().get(0);
		assertEquals("urn:be:fgov:identification-namespace", des0.getNamespace());
		assertEquals("urn:be:fgov:ehealth:1.0:certificateholder:person:ssin", des0.getKey());
		
		SamlDesignator des1 = config.getSamlDesignator().get(1);
		assertEquals("urn:be:fgov:identification-namespace", des1.getNamespace());
		assertEquals("urn:be:fgov:person:ssin", des1.getKey());
		
		SamlDesignator des2 = config.getSamlDesignator().get(2);
		assertEquals("urn:be:fgov:certified-namespace:ehealth", des2.getNamespace());
		assertEquals("urn:be:fgov:person:ssin:doctor:boolean", des2.getKey());
		
		SamlDesignator des3 = config.getSamlDesignator().get(3);
		assertEquals("urn:be:fgov:certified-namespace:ehealth", des3.getNamespace());
		assertEquals("urn:be:fgov:person:ssin:ehealth:1.0:doctor:nihii11", des3.getKey());
		
		StoreDataEntriesRequest storeRequest = dataEntryRequestCapture.getValue();
		assertEquals("89102116775", storeRequest.getSubjectID());
		DataEntry dataEntry = storeRequest.getDataEntries().get(0);
		assertEquals("/subject/89102116775/medication-scheme/new", dataEntry.getDataEntryURI());
		assertEquals((Integer)29, dataEntry.getNodeVersion());
		Map<String, String> metadata = dataEntry.getMetadata();
		assertEquals("encrypted", metadata.get("encryptionFlag"));
		assertEquals("KMEHR_20120401", metadata.get("formatCode"));
		assertEquals("active", metadata.get("availabilityStatus"));
		assertEquals("text/xml", metadata.get("mimeType"));
		assertEquals("nl-BE", metadata.get("languageCode"));
		assertEquals("validated", metadata.get("validationStatus"));
		assertTrue("Businessdata should not be null", dataEntry.getBusinessData() != null);
		assertTrue("Businessdata should not be empty", dataEntry.getBusinessData().length > 0);
		
		RemoveDataEntryRequest removeRequest = removeRequestCapture.getValue();
		assertEquals((Integer)30, removeRequest.getNodeVersion());
		assertEquals("/subject/89102116775/medication-scheme/41693/1", removeRequest.getURI());
	}
}
