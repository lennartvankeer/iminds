//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.01.13 at 01:12:04 PM CET 
//


package be.healthconnect.iminds.vitalinkScenario;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for session complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="session">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authenticationConfigLocation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice maxOccurs="unbounded">
 *           &lt;element name="removeAction" type="{}removeAction"/>
 *           &lt;element name="storeAction" type="{}storeAction"/>
 *           &lt;element name="phvStoreAction" type="{}phvStoreAction"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "session", propOrder = {
    "authenticationConfigLocation",
    "removeActionOrStoreActionOrPhvStoreAction"
})
public class Session {

    @XmlElement(required = true)
    protected String authenticationConfigLocation;
    @XmlElements({
        @XmlElement(name = "storeAction", type = StoreAction.class),
        @XmlElement(name = "phvStoreAction", type = PhvStoreAction.class),
        @XmlElement(name = "removeAction", type = RemoveAction.class)
    })
    protected List<Action> removeActionOrStoreActionOrPhvStoreAction;

    /**
     * Gets the value of the authenticationConfigLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthenticationConfigLocation() {
        return authenticationConfigLocation;
    }

    /**
     * Sets the value of the authenticationConfigLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthenticationConfigLocation(String value) {
        this.authenticationConfigLocation = value;
    }

    /**
     * Gets the value of the removeActionOrStoreActionOrPhvStoreAction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the removeActionOrStoreActionOrPhvStoreAction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoveActionOrStoreActionOrPhvStoreAction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StoreAction }
     * {@link PhvStoreAction }
     * {@link RemoveAction }
     * 
     * 
     */
    public List<Action> getRemoveActionOrStoreActionOrPhvStoreAction() {
        if (removeActionOrStoreActionOrPhvStoreAction == null) {
            removeActionOrStoreActionOrPhvStoreAction = new ArrayList<Action>();
        }
        return this.removeActionOrStoreActionOrPhvStoreAction;
    }

}
