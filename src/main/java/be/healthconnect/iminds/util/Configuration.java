package be.healthconnect.iminds.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configuration {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Configuration.class.getCanonicalName());

	private static final String ENV_PLACEHOLDER = "%ENV%";

	/** The Constant DEFAULT_CONFIG. */
	public static final String DEFAULT_CONFIG = "/application.properties";
	
	/** The Constant ENVIRONMENTAL_SPECIFIC_CONFIG. */
	public static final String ENVIRONMENTAL_SPECIFIC_CONFIG = "/application-%ENV%.properties";

	/** The Constant TEST_CONFIG. */
	public static final String TEST_CONFIG = "/test.properties";

	/** The properties. */
	private static Properties properties = null;

	// /** The Constant Configuration_Config. */
	// public static final String Configuration_CONFIG =
	// "/configuration.properties";

	/**
	 * Gets the property by Key from the DEFAULT_CONFIG.
	 * 
	 * @param key
	 *            the key
	 * @param defaultValue
	 *            the default value
	 * @return the property
	 */
	public static String getProperty(final String key, final String defaultValue) {
		return getPropertyFromConfig(key, defaultValue);
	}

	/**
	 * Gets the long property.
	 * 
	 * @param key
	 *            the key
	 * @param defaultValue
	 *            the default value
	 * @return the long property
	 */
	public static Long getLongProperty(final String key, final Long defaultValue) {
		return Long.valueOf(getPropertyFromConfig(key, "" + defaultValue));
	}

	/**
	 * Gets the integer property.
	 * 
	 * @param key
	 *            the key
	 * @param defaultValue
	 *            the default value
	 * @return the integer property
	 */
	public static Integer getIntegerProperty(final String key, final Integer defaultValue) {
		return Integer.valueOf(getPropertyFromConfig(key, "" + defaultValue));
	}

	/**
	 * Gets the boolean property.
	 * 
	 * @param key
	 *            the key
	 * @param defaultValue
	 *            the default value
	 * @return the boolean property
	 */
	public static Boolean getBooleanProperty(final String key, final Boolean defaultValue) {
		return Boolean.valueOf(getPropertyFromConfig(key, "" + defaultValue));
	}

	/**
	 * Gets the property by Key from the DEFAULT_CONFIG.
	 * 
	 * @param key
	 *            the key
	 * @return the property
	 */
	public static String getProperty(final String key) {
		return getPropertyFromConfig(key, null);
	}

	/**
	 * Gets the property from config.
	 * 
	 * @param key
	 *            the key
	 * @param defaultValue
	 *            the default value
	 * @return the property from config
	 */
	private static String getPropertyFromConfig(final String key, final String defaultValue) {
		if (key == null) {
			return defaultValue;
		}
		try {
			Properties p = getProperties();
			if (!p.containsKey(key)) {
				LOG.log(Level.WARNING, "Undefined config property " + key);
			}
			return p.getProperty(key, defaultValue);
		} catch (IOException e) {
			LOG.log(Level.WARNING, "IOException : " + e.getMessage() + ", returning default value", e);
			return defaultValue;
		}
	}

	/**
	 * Property contains key.
	 * 
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	public static boolean containsKey(String key) {
		try {
			return getProperties().containsKey(key);
		} catch (IOException e) {
			LOG.log(Level.WARNING, "IOException : " + e.getMessage() + ", can't load the property");
			return false;
		}
	}

	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static Properties getProperties() throws IOException {
		if (properties == null) {
			properties = new Properties();
			load(DEFAULT_CONFIG);
			load(ENVIRONMENTAL_SPECIFIC_CONFIG);
			load(TEST_CONFIG);
			// load(Configuration_CONFIG);
		}
		return properties;
	}

	/**
	 * Load.
	 * 
	 * @param file
	 *            the file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private static void load(String file) throws IOException {
		file = file.replace(ENV_PLACEHOLDER, Environment.getCurrentEnvironment().getName());
		LOG.log(Level.WARNING, "Trying to load properties: " + file);
		InputStream is = Configuration.class.getResourceAsStream(file);
		if (is != null) {
			properties.load(is);
			LOG.log(Level.WARNING, "Loaded properties: " + file);
		} else {
			LOG.log(Level.SEVERE, "Loading properties failed: " + file);
		}
	}

	/**
	 * Gets the uRL property.
	 * 
	 * @param property
	 *            the string
	 * @return the uRL property
	 * @throws MalformedURLException
	 */
	public static URL getURLProperty(String property) {
		String wsdl = getProperty(property);
		if (wsdl == null) {
			return null;
		}

		File f = new File(wsdl);
		URL url = null;
		try {
			url = f.toURI().toURL();
		} catch (MalformedURLException e) {
			LOG.severe(e.getMessage());
		}
		return url;
	}

	/**
	 * Method to override a property at runtime
	 * 
	 * @param key
	 * @return value
	 */
	public static void setProperty(String key, String value) {
		try {
			getProperties().remove(key);
			getProperties().put(key, value);
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Could not set property: [" + key + "] with value [" + value + "]");
			LOG.severe(e.getMessage());
		}
	}
}
