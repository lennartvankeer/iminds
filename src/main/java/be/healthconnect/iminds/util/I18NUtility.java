package be.healthconnect.iminds.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class I18NUtility {
	private static final Logger LOG = Logger.getLogger(I18NUtility.class.getName());	
	private static final String BUNDLE_NAME = "be.healthconnect.iminds.messages";
	
	private static ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault());
	
	
	/**
	 * Returns the localized message for the given key.
	 * 
	 * @param key
	 *            the message key
	 * @return the localized message
	 */
	public static String getMessage(String key) {
		return lookupMsg(getResourceBundle(), key, new Object[0]);
	}
	
	private static String lookupMsg(ResourceBundle bundle, String key, Object[] context) {
		try {
			String msg = bundle.getString(key).replace("\\n", "\n");
			if (context != null && context.length > 0) {
				msg = MessageFormat.format(msg, context);
			}
			return msg;
		} catch (MissingResourceException mre) {
			LOG.warning("Key not found in resource bundles: " + key + ". Returning key as label.");
			return "???" + key + "???";
		}
	}

	private static ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	/**
	 * Returns the localized message for the given key.
	 * 
	 * @param key
	 *            the message key
	 * @param context
	 *            any placeholder objects to use in the message
	 * @return the localized message
	 */
	public static String getMessage(String key, Object... context) {
		return lookupMsg(getResourceBundle(), key, context);
	}
	
	
	
}
