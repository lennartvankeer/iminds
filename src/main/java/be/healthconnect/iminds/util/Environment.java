package be.healthconnect.iminds.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Environment {
	/** The DEVELOPMENT environment. */
	DEVELOPMENT("dev"),
	
	/** The DEVELOPMENT environment for continuous deployment. */
	DEVELOPMENT_CONT_DEPLOY("devcd"),
	
	/** The INTEGRATION environment. */
	INTEGRATION("intg"),
	
	/** The ACCEPTANCE environment. */
	ACCEPTANCE("acc"),
	
	/** The PRODUCTION environment. */
	PRODUCTION("prod");
	
	/** The name. */
	private String name;
	
	/** The lookup map. */
	private static Map<String,Environment> lookup;
	
	/** The Constant ENV_PARAMETER. */
	public static final String ENV_PARAMETER = "env";
	
	/** The Constant currentEnvironment. */
	private static Environment currentEnvironment;
	
	static {
		buildLookupMap();
		final String env = System.getProperty(Environment.ENV_PARAMETER, Environment.getDefaultenvironment().getName());
		currentEnvironment = lookup.get(env);		
	}
	
	/**
	 * Instantiates a new environment.
	 * 
	 * @param name the name
	 */
	private Environment(String name) {
		this.name = name;
	}
	
	public static void init(String env){
		currentEnvironment = lookup.get(env);
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the environment.
	 * 
	 * @param name the name
	 * @return the environment
	 */
	public static Environment lookup(String name) {
		if (lookup.containsKey(name)) {
			return lookup.get(name);
		}
		return getDefaultenvironment();
	}
	
	/**
	 * Gets the current environment.
	 * 
	 * @return the current environment
	 */
	public static Environment getCurrentEnvironment() {		
		return currentEnvironment;
	}
	
	/**
	 * Gets the default environment.
	 * 
	 * @return the defaultenvironment
	 */
	public static Environment getDefaultenvironment() {
		return DEVELOPMENT;
	}
	
	/**
	 * Builds the lookup map.
	 */
	private static void buildLookupMap() {
		lookup = new HashMap<String,Environment>();
		for (Environment env : EnumSet.allOf(Environment.class)) {
			lookup.put(env.getName(), env);	
		}
	}
}
