package be.healthconnect.iminds.util.credentials;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import be.ehealth.technicalconnector.exception.TechnicalConnectorException;
import be.ehealth.technicalconnector.service.sts.security.Credential;

public class KeyStoreCredential implements Credential{
	private final KeyStore keystore;	
	private static final String ALIAS = "authentication";
	private final PrivateKey privateKey;
	
	public KeyStoreCredential(String location, String password) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException {
		FileInputStream fIn = new FileInputStream(location);
	    keystore = KeyStore.getInstance("PKCS12");

	    keystore.load(fIn, password.toCharArray());
	    privateKey = (PrivateKey) keystore.getKey(ALIAS, password.toCharArray());
	}
	
	@Override
	public X509Certificate getCertificate() throws TechnicalConnectorException {
		try {
			return (X509Certificate) keystore.getCertificate(ALIAS);
		} catch (KeyStoreException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Certificate[] getCertificateChain() throws TechnicalConnectorException {
		try {
			return keystore.getCertificateChain(ALIAS);
		} catch (KeyStoreException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getIssuer() throws TechnicalConnectorException {
		return getCertificate().getIssuerDN().toString();		
	}

	@Override
	public String getIssuerQualifier() throws TechnicalConnectorException {
		return getCertificate().getIssuerUniqueID().toString();		
	}

	@Override
	public KeyStore getKeyStore() throws TechnicalConnectorException {
		return keystore;
	}

	@Override
	public PrivateKey getPrivateKey() throws TechnicalConnectorException {
		return privateKey;
	}

	@Override
	public String getProviderName() throws TechnicalConnectorException {
		return keystore.getProvider().getName();
	}

	@Override
	public PublicKey getPublicKey() throws TechnicalConnectorException {
		return getCertificate().getPublicKey();
	}
}
