package be.healthconnect.iminds.util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import be.healthconnect.iminds.authenticationConfig.AuthenticationConfig;
import be.healthconnect.iminds.vitalinkScenario.Scenario;

public class UnmarshallerHelper {

	private static Unmarshaller scenarioUnmarshaller;
	private static Unmarshaller authConfigUnmarshaller;
	
	public synchronized static Scenario unmarshalScenario(File scenarioFile) throws JAXBException{
		if(scenarioUnmarshaller == null){
			JAXBContext jaxbContext = JAXBContext.newInstance(Scenario.class);
			scenarioUnmarshaller = jaxbContext.createUnmarshaller();
		}
		return (Scenario) scenarioUnmarshaller.unmarshal(scenarioFile);		
	}
	
	public synchronized static AuthenticationConfig unmarshalAuthConfig(File authConfigFile) throws JAXBException{
		if(authConfigUnmarshaller == null){
			JAXBContext jaxbContext = JAXBContext.newInstance(AuthenticationConfig.class);
			authConfigUnmarshaller = jaxbContext.createUnmarshaller();
		}
		return (AuthenticationConfig) authConfigUnmarshaller.unmarshal(authConfigFile);		
	}
}
