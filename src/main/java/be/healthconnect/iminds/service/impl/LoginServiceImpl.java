package be.healthconnect.iminds.service.impl;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import be.ehealth.technicalconnector.config.ConfigFactory;
import be.ehealth.technicalconnector.config.ConfigValidator;
import be.ehealth.technicalconnector.config.impl.ConfigValidatorImpl;
import be.ehealth.technicalconnector.exception.SessionManagementException;
import be.ehealth.technicalconnector.exception.TechnicalConnectorException;
import be.ehealth.technicalconnector.service.sts.SAMLTokenFactory;
import be.ehealth.technicalconnector.service.sts.domain.SAMLAttribute;
import be.ehealth.technicalconnector.service.sts.domain.SAMLAttributeDesignator;
import be.ehealth.technicalconnector.service.sts.impl.STSServiceImpl;
import be.ehealth.technicalconnector.service.sts.security.SAMLToken;
import be.ehealth.technicalconnector.session.Session;
import be.healthconnect.iminds.authenticationConfig.AuthenticationConfig;
import be.healthconnect.iminds.authenticationConfig.SamlAttribute;
import be.healthconnect.iminds.authenticationConfig.SamlDesignator;
import be.healthconnect.iminds.service.LoginService;
import be.healthconnect.iminds.util.credentials.KeyStoreCredential;
import be.smals.safe.common.global.credential.Credential;

public class LoginServiceImpl implements LoginService{

	@Override
	public void doLogin(AuthenticationConfig authenticationConfig) throws SessionManagementException, TechnicalConnectorException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		if (Session.getInstance().hasValidSession()) {
			Session.getInstance().unloadSession();
		}		
		
		switch (authenticationConfig.getType()) {
		case EID:
			createEidSession(authenticationConfig);
			break;
		case FALL_BACK:			
			createFallBackSession(authenticationConfig);
			break;
		default:
			throw new IllegalArgumentException("The type should be 'eid' or 'fallBack'");
		}
	}
	
	private void createEidSession(AuthenticationConfig authenticationConfig) throws SessionManagementException, TechnicalConnectorException{
		ConfigValidator configValidator = ConfigFactory.getConfigValidator();
		
		for(int i = 0; i < authenticationConfig.getSamlAttribute().size(); i++){
			SamlAttribute attr = authenticationConfig.getSamlAttribute().get(i);
			StringBuilder sb = new StringBuilder(attr.getNamespace());
			sb.append(",");
			sb.append(attr.getKey());
			sb.append(",");
			sb.append(attr.getValue());
			configValidator.setProperty("sessionmanager.samlattribute."+(i+1),sb.toString()); 
		}	
		
		
		for(int i = 0; i < authenticationConfig.getSamlDesignator().size(); i++){
			SamlDesignator designator = authenticationConfig.getSamlDesignator().get(i);
			StringBuilder sb = new StringBuilder(designator.getNamespace());
			sb.append(",");
			sb.append(designator.getKey());
			configValidator.setProperty("sessionmanager.samlattributedesignator."+(i+1),sb.toString()); 			
		}
		configValidator.setProperty("user.ssin", authenticationConfig.getSsin());
		configValidator.setProperty("sessionmanager.identification.keystore", authenticationConfig.getMainCertificateLocation());	
		configValidator.setProperty("sessionmanager.holderofkey.keystore",authenticationConfig.getMainCertificateLocation());
		configValidator.setProperty("sessionmanager.encryption.keystore",authenticationConfig.getMainCertificateLocation());
		
		
		Session.getInstance().createSessionEidOnly();
	}
	
	private void createFallBackSession(AuthenticationConfig authenticationConfig) throws SessionManagementException, TechnicalConnectorException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
		KeyStoreCredential authCredential = new KeyStoreCredential(authenticationConfig.getMainCertificateLocation(), authenticationConfig.getMainCertificatePassword());
		KeyStoreCredential sessionCredential;
		if(authenticationConfig.getSecondaryCertificateLocation() == null){
			sessionCredential = authCredential;
		}else{
			sessionCredential= new KeyStoreCredential(authenticationConfig.getSecondaryCertificateLocation(), authenticationConfig.getSecondaryCertificatePassword());
		}
		STSServiceImpl stsService  = new STSServiceImpl();
		
		List<SAMLAttribute> attributes = new ArrayList<>();
		authenticationConfig.getSamlAttribute().forEach(attr -> {
			attributes.add(new SAMLAttribute(attr.getKey(), attr.getNamespace(), attr.getValue()));
		});
		List<SAMLAttributeDesignator> designators = new ArrayList<>();
		authenticationConfig.getSamlDesignator().forEach(designator -> {
			designators.add(new SAMLAttributeDesignator(designator.getKey(), designator.getNamespace()));
		});
		
		Element assertion = stsService.getToken(authCredential, sessionCredential, attributes, designators, "urn:oasis:names:tc:SAML:1.0:cm:holder-of-key", 24);
		
		SAMLToken token = SAMLTokenFactory.getInstance().createSamlToken(assertion, sessionCredential);			
		
		Session.getInstance().loadSession(token, null);		
	}

}
