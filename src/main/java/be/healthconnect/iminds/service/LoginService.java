package be.healthconnect.iminds.service;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import be.ehealth.technicalconnector.exception.SessionManagementException;
import be.ehealth.technicalconnector.exception.TechnicalConnectorException;
import be.healthconnect.iminds.authenticationConfig.AuthenticationConfig;

public interface LoginService {

	void doLogin(AuthenticationConfig authenticationConfig) throws SessionManagementException, TechnicalConnectorException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException;
}
