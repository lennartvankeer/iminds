package be.healthconnect.iminds.di;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import com.google.inject.AbstractModule;

import be.healthconnect.iminds.IMindsTestToolApp;
import be.healthconnect.iminds.controller.LogController;
import be.healthconnect.iminds.controller.NavigationController;
import be.healthconnect.iminds.controller.RootController;
import be.healthconnect.iminds.controller.VitalinkScenarioController;
import be.healthconnect.iminds.controller.impl.LogControllerImpl;
import be.healthconnect.iminds.controller.impl.NavigationControllerImpl;
import be.healthconnect.iminds.controller.impl.RootControllerImpl;
import be.healthconnect.iminds.controller.impl.VitalinkScenarioControllerImpl;
import be.healthconnect.iminds.service.LoginService;
import be.healthconnect.iminds.service.impl.LoginServiceImpl;
import be.healthconnect.iminds.view.LogView;
import be.healthconnect.iminds.view.NavigationView;
import be.healthconnect.iminds.view.Root;
import be.healthconnect.iminds.view.RootView;
import be.healthconnect.iminds.view.ViewManager;
import be.healthconnect.iminds.view.VitalinkScenarioView;
import be.healthconnect.iminds.view.fx.FXLogView;
import be.healthconnect.iminds.view.fx.FXNavigationView;
import be.healthconnect.iminds.view.fx.FXRootView;
import be.healthconnect.iminds.view.fx.FXVitalinkScenarioView;
import be.healthconnect.iminds.view.impl.ViewManagerImpl;
import be.smals.safe.connector.VitalinkService;
import javafx.stage.Stage;

public class IMindsTestToolModule extends AbstractModule{

	@Override
	protected void configure() {
		/*
		 * General
		 */
		bind(ViewManager.class).to(ViewManagerImpl.class);
		bind(IMindsTestToolApp.class).toInstance(IMindsTestToolApp.getInstance());
		bind(Stage.class).annotatedWith(Root.class).toInstance(IMindsTestToolApp.getInstance().getStage());

		
		//Services
		bind(LoginService.class).to(LoginServiceImpl.class);
		bind(VitalinkService.class).toInstance(new VitalinkService());
		bind(ExecutorService.class).toInstance(Executors.newCachedThreadPool(new ThreadFactory() {
			private int counter = 0;

			@Override
			public Thread newThread(final Runnable task) {
				final Thread t = new Thread(task);
				t.setName("Controller background thread-" + ++counter);
				t.setDaemon(true);
				t.setPriority(Thread.NORM_PRIORITY - 1);
				return t;
			}
		}));
		
		//Controllers		
		bind(RootController.class).to(RootControllerImpl.class);
		bind(NavigationController.class).to(NavigationControllerImpl.class);
		bind(VitalinkScenarioController.class).to(VitalinkScenarioControllerImpl.class);
		bind(LogController.class).to(LogControllerImpl.class);
		
		//Views
		bind(RootView.class).to(FXRootView.class);
		bind(NavigationView.class).to(FXNavigationView.class);
		bind(VitalinkScenarioView.class).to(FXVitalinkScenarioView.class);
		bind(LogView.class).to(FXLogView.class);
	}

}
