package be.healthconnect.iminds.controller.impl;

import com.google.inject.Inject;

import be.healthconnect.iminds.controller.NavigationController;
import be.healthconnect.iminds.controller.RootController;
import be.healthconnect.iminds.view.RootView;

public class RootControllerImpl implements RootController{

	private final RootView rootView;
	private final NavigationController navigationController;
	
	@Inject
	public RootControllerImpl(RootView rootView, NavigationController navigationController){
		this.rootView = rootView;
		this.navigationController = navigationController;
	}
	
	public void activate() {
		rootView.attach();
		navigationController.activate();
	}

	public void deactivate() {
		throw new UnsupportedOperationException("The rootController can not be deactivated");
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

}
