package be.healthconnect.iminds.controller.impl;

import be.healthconnect.iminds.controller.Controller;

public class ActiveControllerHelper {
	private Controller activeController;
	
	public void activateController(Controller controller){
		if(activeController == controller){
			return;
		}
		if(activeController != null){
			activeController.deactivate();
		}
		activeController = controller;
		activeController.activate();
	}
}
