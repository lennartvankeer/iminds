package be.healthconnect.iminds.controller.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import be.smals.safe.connector.domain.DataEntry;

public class PHVFileReader{

	private final BufferedReader reader;
	
	public PHVFileReader(String fileLocation) throws FileNotFoundException{
		File file = new File(fileLocation);
		FileInputStream fis = new FileInputStream(file);
		 
		reader = new BufferedReader(new InputStreamReader(fis));	
	}
	
	/**
	 * reads the next dataEntry and returns it
	 * the nodeversion is always null as it is not present in a phv file 
	 * (the nodeversion is not there because the PHV file was never intended for this purpose, it is actually a debugging tool in PHV)
	 * @return the read dataEntry or null if no more dataEntries are to be read
	 * @throws IOException 
	 */
	public DataEntry readDataEntry() throws IOException{
		String uri = reader.readLine();
		if(uri == null){
			return null;
		}
		DataEntry result = new DataEntry(uri);
		result.getMetadata().putAll(parseMetaDataString(reader.readLine()));
		result.setBusinessData(readBusinessData());
		String line = reader.readLine();
		if(!line.equals("------------------------")){ 
			throw new IllegalArgumentException("the PHVFile is is corrupt, alter each dataentry a line with '------------------------' should be present");
		}
		return result;
	}

	private byte[] readBusinessData() throws IOException {
		StringBuilder readString = new StringBuilder();
		while(!readString.toString().endsWith("</kmehrmessage>")){
			String line = reader.readLine();
			if(line.equals("------------------------")){//in this case the kmehr is not a valid xml 
				throw new IllegalArgumentException("the kmehrmessage didn't end with a closing tag </kmehrmessage>");
			}else{
				readString.append(line);
			}
		}		
		return readString.toString().getBytes();
	}

	private Map<String, String> parseMetaDataString(String readLine) {
		Map<String, String> result = new HashMap<>();
		String metaDataLine = readLine.substring(1, readLine.length()-2); // strip '{' and '}'
		String[] metaData = metaDataLine.split(", ");
		for(String metaDataPair: metaData){
			String[] metaDataPairSplit = metaDataPair.split("=");
			result.put(metaDataPairSplit[0], metaDataPairSplit[1]);
		}
		return result;
	}
	
	

}
