package be.healthconnect.iminds.controller.impl;

import java.util.logging.Logger;

import com.google.inject.Inject;

import be.healthconnect.iminds.controller.LogController;
import be.healthconnect.iminds.controller.NavigationController;
import be.healthconnect.iminds.controller.VitalinkScenarioController;
import be.healthconnect.iminds.dto.Navigation;
import be.healthconnect.iminds.view.NavigationView;
import be.healthconnect.iminds.view.NavigationViewListener;

public class NavigationControllerImpl implements NavigationController, NavigationViewListener{
	private static final Logger LOG = Logger.getLogger(NavigationControllerImpl.class.getName());	
	
	private Navigation activeTab;
	private final Navigation initialTab = Navigation.VITALINKSCENARIO;
	
	private final NavigationView view;	
	private final VitalinkScenarioController vitalinkScenarioController;
	private final LogController logController;
	private final ActiveControllerHelper activeControllerHelper;

	@Inject
	public NavigationControllerImpl(NavigationView view, VitalinkScenarioController vitalinkScenarioController, LogController logController) {
		this.view = view;
		this.vitalinkScenarioController = vitalinkScenarioController;
		this.logController = logController;
		
		activeControllerHelper = new ActiveControllerHelper();
	}
	
	@Override
	public void activate() {
		view.attach();
		view.addListener(this);
		if(activeTab == null){
			activeTab = initialTab;
		}
		navigateTo(activeTab);
	}

	@Override
	public void deactivate() {
		view.detach();		
	}

	@Override
	public void navigateTo(Navigation navigateTo) {
		activeTab = navigateTo;
		switch (navigateTo) {
		case VITALINKSCENARIO:
			activeControllerHelper.activateController(vitalinkScenarioController);
			break;
		case LOG:
			activeControllerHelper.activateController(logController);
			break;
		default:LOG.warning("trying to navigate to %s, however the navigation is not implemented");
			break;
		}
		view.setActiveTab(navigateTo);
		
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

}
