/**
 * (C) 2012-2015 HealthConnect NV. All rights reserved.
 */
package be.healthconnect.iminds.controller.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;


import be.healthconnect.iminds.concurrent.CallResponse;
import be.healthconnect.iminds.controller.Controller;
import be.healthconnect.iminds.view.View;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker.State;

/**
 * Abstract controller base class.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public abstract class ControllerImpl implements Controller {

	protected static final Logger LOG = Logger.getLogger(ControllerImpl.class.getName());
	protected static final Logger ASYNC_LOG = Logger.getLogger("ASYNC_LOG");

	protected final CallResponse<Throwable> onFailure;

	private ExecutorService executor;
	private boolean active;	

	/**
	 * Creates a new {@link ControllerImpl}.
	 * 
	 * @param injector
	 *            the dependency injector to use for creating dynamic instances
	 * @param context
	 *            the global application context
	 * @param dialogs
	 *            the {@link Dialogs} utility to use
	 * @param view
	 *            the main view controlled by this controller
	 */
	@Inject
	public ControllerImpl(final View view) {
		super();		
		this.onFailure = (Throwable exception) -> {
			logException(exception, Level.SEVERE);
			
		};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activate() {
		setActive(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deactivate() {
		setActive(false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets whether this controller is active.
	 * 
	 * @param active
	 *            the active state to set
	 */
	protected void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Performs <code>task</code> asynchronously.
	 * 
	 * @param task
	 *            the task to perform asynchronously
	 * @param onSuccess
	 *            the response to execute on successful execution of the task
	 * @param onFailure
	 *            the response to execute on task failure
	 */
	protected <T> void doAsync(final Callable<T> task, final CallResponse<T> onSuccess, final CallResponse<Throwable> onFailure) {
		final Task<T> fxTask = new Task<T>() {
			@Override
			protected T call() throws Exception {
				return task.call();				
			}
		};
		fxTask.stateProperty().addListener((ObservableValue<? extends State> observable, State oldValue, State newValue) -> {
			switch (newValue) {
			case SUCCEEDED:
				if (onSuccess != null) {
					try {
						onSuccess.response(fxTask.getValue());
					} catch (Exception exception) {
						if (onFailure != null) {
							onFailure.response(exception);
						} else {
							logException(exception, Level.SEVERE);
						}
					}
				}
				break;
			case CANCELLED:
			case FAILED:
				final Throwable exception = fxTask.getException();
				
					if (onFailure != null) {
						onFailure.response(exception);
					} else {
						logException(exception, Level.SEVERE);
					}
				
				break;
			default:
				break;
			}
		});
		getExecutor().execute(fxTask);
	}

	/**
	 * Performs <code>task</code> asynchronously for tasks that can only be performed in online mode.
	 * 
	 * @param task
	 *            the task to perform asynchronously
	 * @param onSuccess
	 *            the response to execute on successful execution of the task
	 * @param onFailure
	 *            the response to execute on task failure
	 */
	protected <T> void doAsyncOnline(final Callable<T> task, final CallResponse<T> onSuccess, final CallResponse<Throwable> onFailure) {
		final Task<T> fxTask = new Task<T>() {
			@Override
			protected T call() throws Exception {				
				return task.call();
				
			}
		};
		fxTask.stateProperty().addListener((ObservableValue<? extends State> observable, State oldValue, State newValue) -> {
			switch (newValue) {
			case SUCCEEDED:
				if (onSuccess != null) {
					try {
						onSuccess.response(fxTask.getValue());
					} catch (Exception exception) {
						if (onFailure != null) {
							onFailure.response(exception);
						} else {
							logException(exception, Level.SEVERE);
						}
					}
				}
				break;
			case CANCELLED:
			case FAILED:
				final Throwable exception = fxTask.getException();
				if (onFailure != null) {
					onFailure.response(exception);
				} else {
					logException(exception, Level.SEVERE);
				}
				break;
			default:
				break;
			}
		});
		getExecutor().execute(fxTask);
	}

	/**
	 * Performs <code>task</code> asynchronously. Displays a standard error message on failure.
	 * 
	 * @param task
	 *            the task to perform asynchronously
	 * @param onSuccess
	 *            the response to execute on successful execution of the task
	 */
	protected <T> void doAsync(final Callable<T> task, final CallResponse<T> onSuccess) {
		doAsync(task, onSuccess, onFailure);
	}

	/**
	 * <p>
	 * Performs <code>task</code> asynchronously. Returns a {@link Future} to the returned result. The {@link Future} will throw an
	 * {@link ExecutionException} on {@link Future#get()} when an exception other than a connection exception was thrown during the
	 * execution of <code>task</code>.
	 * </p>
	 * <p>
	 * <b>NOTE:</b> You MUST call {@link Future#get()} on the returned {@link Future} in order to receive any error notification!
	 * </p>
	 * 
	 * @param task
	 *            the task to perform asynchronously
	 * @return a {@link Future} to the returned result
	 */
	protected <T> Future<T> doAsync(final Callable<T> task) {
		final Callable<T> retryOfflineWrapper = () -> {
			
				try {
					final T t = task.call();
					return t;
				} catch (Throwable e) {
					logException(e, Level.SEVERE);
					throw e;
					
				}
			
		};
		return getExecutor().submit(retryOfflineWrapper);
	}

	

	/**
	 * Performs <code>task</code> synchronously.
	 * 
	 * @param task
	 *            the task to perform synchronously
	 */
	protected void doSync(final Runnable task) {
		Platform.runLater(task);
	}

	

	/**
	 * Returns the background thread executor service.
	 * 
	 * @return the background thread executor service
	 */
	protected ExecutorService getExecutor() {
		return executor;
	}

	/**
	 * Sets the background thread executor service.
	 * 
	 * @param executor
	 *            the executor to set
	 */
	@Inject
	protected void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}

	/**
	 * Logs <code>exception</code> at <code>level</code>.
	 * 
	 * @param exception
	 *            the exception to log
	 * @param level
	 *            the logging level
	 */
	protected void logException(final Throwable exception, final Level level) {
		LOG.log(level, exception.getMessage(), exception);
	}

	

	

}