package be.healthconnect.iminds.controller.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.inject.Inject;

import be.healthconnect.iminds.authenticationConfig.AuthenticationConfig;
import be.healthconnect.iminds.controller.VitalinkScenarioController;
import be.healthconnect.iminds.exception.ResponseStatusNotSuccessException;
import be.healthconnect.iminds.service.LoginService;
import be.healthconnect.iminds.util.I18NUtility;
import be.healthconnect.iminds.util.UnmarshallerHelper;
import be.healthconnect.iminds.view.VitalinkScenarioView;
import be.healthconnect.iminds.view.VitalinkScenarioViewListener;
import be.healthconnect.iminds.vitalinkScenario.Action;
import be.healthconnect.iminds.vitalinkScenario.Dataentry;
import be.healthconnect.iminds.vitalinkScenario.PhvStoreAction;
import be.healthconnect.iminds.vitalinkScenario.RemoveAction;
import be.healthconnect.iminds.vitalinkScenario.Scenario;
import be.healthconnect.iminds.vitalinkScenario.Session;
import be.healthconnect.iminds.vitalinkScenario.StoreAction;
import be.smals.safe.connector.VitalinkService;
import be.smals.safe.connector.domain.DataEntry;
import be.smals.safe.connector.domain.OrganizationInformation;
import be.smals.safe.connector.domain.PersonInformation;
import be.smals.safe.connector.domain.Status;
import be.smals.safe.connector.domain.protocol.RemoveDataEntryRequest;
import be.smals.safe.connector.domain.protocol.RemoveDataEntryResponse;
import be.smals.safe.connector.domain.protocol.StoreDataEntriesRequest;
import be.smals.safe.connector.domain.protocol.StoreDataEntriesResponse;
import javafx.application.Platform;

public class VitalinkScenarioControllerImpl extends ControllerImpl implements VitalinkScenarioController, VitalinkScenarioViewListener{
	private static final Logger LOG = Logger.getLogger(VitalinkScenarioControllerImpl.class.getName());	
	
	
	private final VitalinkScenarioView view;
	private final LoginService loginService;
	private final VitalinkService vitalinkService;

	@Inject
	public VitalinkScenarioControllerImpl(VitalinkScenarioView view, LoginService loginService, VitalinkService vitalinkService) {
		super(view);
		this.view = view;
		this.loginService = loginService;
		this.vitalinkService = vitalinkService;
	}
	
	@Override
	public void activate() {
		view.attach();
		view.addListener(this);
	}

	@Override
	public void deactivate() {
		view.detach();
		
	}

	@Override
	public void runTest(File scenarioFile) {
		if(scenarioFile == null){
			view.inform(I18NUtility.getMessage("vitalinkscenario.error.nofile"));
		}
		view.inform(I18NUtility.getMessage("vitalinkscenario.started"));
		doAsync(()->{
			Scenario scenario = UnmarshallerHelper.unmarshalScenario(scenarioFile);			
			view.inform(I18NUtility.getMessage("vitalinkscenario.scenarioparsed"));
			for(Session session: scenario.getSession()){
				runSession(session);
			}
			return null;
		},(nothing)->{
			view.inform(I18NUtility.getMessage("vitalinkscenario.stopped.success"));
		}, (ex)->{
			LOG.error("test ended with exception", ex);			
			view.inform(I18NUtility.getMessage("vitalinkscenario.stopped.exception", ex));			
		});
	}

	private void runSession(final Session session) throws Exception {
		login(session.getAuthenticationConfigLocation());
		Collections.sort(session.getRemoveActionOrStoreActionOrPhvStoreAction(), 
			(action1, action2)-> action1.getIndex().compareTo(action2.getIndex())
		);
		for(Action action : session.getRemoveActionOrStoreActionOrPhvStoreAction()){
			runAction(action);
		};			
	}

	
	

	private Action runAction(Action action) throws IOException {
		if(action instanceof StoreAction){
			store((StoreAction) action);
		}else if(action instanceof RemoveAction){
			remove((RemoveAction) action);
		}else if(action instanceof PhvStoreAction){
			phvStore((PhvStoreAction) action);
		}else{
			throw new IllegalArgumentException("the action is of a type that is not yet implemented");
		}
		return action;
	}
	
	
	private void phvStore(PhvStoreAction action) throws IOException {
		PHVFileReader reader = new PHVFileReader(action.getPhvFileLocation());
		int nodeVersion = Integer.valueOf(action.getNodeversion());
		String subjectSsin = action.getSubjectssin();
		DataEntry dataEntry;		
		int index = 1; 
		List<DataEntry> dataEntries = new ArrayList<>();
		while((dataEntry = reader.readDataEntry()) != null){
			DataEntry newDataEntry = createDataEntry(dataEntry, subjectSsin, nodeVersion + (int)(index / 25));
			index++;
			dataEntries.add(newDataEntry);
		}
		for(int i = 0; i<dataEntries.size(); i+=25){
			StoreDataEntriesRequest request = new StoreDataEntriesRequest(action.getSubjectssin());			
			for(int j = 0; j<25 && j+i < dataEntries.size(); j++){
				request.setSubjectID(action.getSubjectssin());
				request.getDataEntries().add(dataEntries.get(j));
			}
			StoreDataEntriesResponse response = vitalinkService.storeDataEntries(request);
			doStatusCheck(response.getStatus());
		}
	}

	private void remove(RemoveAction action) {
		RemoveDataEntryRequest request = new RemoveDataEntryRequest(action.getUri());
		request.setNodeVersion(action.getNodeversion().intValue());
		RemoveDataEntryResponse response = vitalinkService.removeDataEntry(request);
		doStatusCheck(response.getStatus());
	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

	private void store(StoreAction action) throws IOException{
		StoreDataEntriesRequest request = new StoreDataEntriesRequest(action.getSubjectssin());
		request.setSubjectID(action.getSubjectssin());
		for(Dataentry dataEntry : action.getDataentry()){		
			DataEntry entry = new DataEntry(dataEntry.getUri());
			entry.setNodeVersion(dataEntry.getNodeversion().intValue());
			entry.setBusinessData(readBusinessData(dataEntry.getPayloadLocation()));
			entry.getMetadata().putAll(readMetaData(dataEntry.getMetadataLocation()));
			entry.setReference(UUID.randomUUID().toString());
			request.getDataEntries().add(entry);			
		};
		StoreDataEntriesResponse response = vitalinkService.storeDataEntries(request);
		doStatusCheck(response.getStatus());
	}

	private void doStatusCheck(Status status){
		view.inform(status.getMessage());
		if(status.getCode() != 200){
			view.inform(I18NUtility.getMessage("vitalinkscenario.error.codeNot200", status.getCode(), status.getMessage()));			
			throw new ResponseStatusNotSuccessException();
		}
	}
	
	private Map<String, String> readMetaData(String metadataLocation) throws IOException{
		Map<String, String> metaData = new HashMap<>();
		Properties prop = new Properties();
		prop.load(new FileInputStream(metadataLocation));			
		prop.entrySet().forEach(entry ->{
			metaData.put((String)entry.getKey(), (String)entry.getValue());
		});
		return metaData;
	}

	private byte[] readBusinessData(String payloadLocation) throws IOException {		
		return IOUtils.toByteArray(new FileInputStream(payloadLocation));		
	}

	private void login(final String authenticationConfigLocation) throws Exception {
		final File authConfigFile = new File(authenticationConfigLocation);
		if(authConfigFile.exists()){
			AuthenticationConfig authenticationConfig = UnmarshallerHelper.unmarshalAuthConfig(authConfigFile);
			loginService.doLogin(authenticationConfig);
			view.inform(I18NUtility.getMessage("vitalinkscenario.loginSuccess"));	
		}else{
			view.inform(I18NUtility.getMessage("vitalinkscenario.error.authFileNotFound", authenticationConfigLocation));
			throw new IllegalStateException();
		}
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	private DataEntry createDataEntry(DataEntry original, String subjectSsin, int nodeVersion){
		String node = original.getDataEntryURI().split("/")[3];		
		
		DataEntry result = new DataEntry("/subject/"+subjectSsin+"/"+node+"/new");
		result.setBusinessData(original.getBusinessData());
		result.getMetadata().putAll(filterMetadata(original.getMetadata()));
		result.setNodeVersion(nodeVersion);
		result.setReference(UUID.randomUUID().toString());
		
		return result;
	}

	private Map<String, String> filterMetadata(Map<String, String> metadata) {
		String[] allowedKeysForStore = {"languageCode", "availabilityStatus", "formatCode", "mimeType", "encryptionFlag", "validationStatus"};
		Map<String, String> result = new HashMap<>();
		
		for(String key: allowedKeysForStore){
			if(metadata.containsKey(key)){
				result.put(key, metadata.get(key));
			}
		}
		return result;
	}
}
