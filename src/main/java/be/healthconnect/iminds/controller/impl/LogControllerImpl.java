package be.healthconnect.iminds.controller.impl;

import com.google.inject.Inject;

import be.healthconnect.iminds.controller.LogController;
import be.healthconnect.iminds.view.LogView;

public class LogControllerImpl implements LogController{

	private final LogView logView;

	@Inject
	public LogControllerImpl(LogView logView) {
		this.logView = logView;
	}
	
	@Override
	public void activate() {
		logView.attach();
	}

	@Override
	public void deactivate() {
		logView.detach();
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

}
