/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.controller;

/**
 * Base interface for all controllers.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public interface Controller {

	/**
	 * Activates this controller. Attaches the view(s) managed by this controller, and also activates any nested controllers.
	 */
	void activate();

	/**
	 * Deactivates this controller. Detaches the view(s) managed by this controller, and also deactivates any nested controllers.
	 */
	void deactivate();

	boolean isActive();

}
