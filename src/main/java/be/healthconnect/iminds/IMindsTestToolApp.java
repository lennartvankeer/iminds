package be.healthconnect.iminds;

import com.google.inject.Guice;
import com.google.inject.Injector;

import be.healthconnect.iminds.controller.Controller;
import be.healthconnect.iminds.di.IMindsTestToolModule;
import be.healthconnect.iminds.util.Configuration;
import be.healthconnect.iminds.util.I18NUtility;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class IMindsTestToolApp extends Application{
	private Stage primaryStage;
	private static IMindsTestToolApp instance;
	
	public static void main(String[] args) {
        launch(args);
    }
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		IMindsTestToolApp.instance = this;
		this.primaryStage = primaryStage;
		
		final Injector injector = Guice
				.createInjector((IMindsTestToolModule) Class.forName(Configuration.getProperty("di.module")).newInstance());

		final Controller root = (Controller) injector.getInstance(Class.forName(Configuration.getProperty("controller.root")));
		root.activate();
		
		primaryStage.setMinWidth(300);
		primaryStage.setMinHeight(300);
		primaryStage.setHeight(300);
		primaryStage.setTitle(I18NUtility.getMessage("app.title"));	
		primaryStage.getIcons().add(new Image("/images/iminds_green.png"));
		primaryStage.setOnHidden(ev -> System.exit(0));
		primaryStage.show();
		primaryStage.toFront();
	}

	public static IMindsTestToolApp getInstance() {		
		return instance;
	}

	public Stage getStage() {		
		return primaryStage;
	}

}
