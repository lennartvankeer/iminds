package be.healthconnect.iminds.view;

import be.healthconnect.iminds.dto.Navigation;

public interface NavigationViewListener {	
	void navigateTo(Navigation navigateTo);
}
