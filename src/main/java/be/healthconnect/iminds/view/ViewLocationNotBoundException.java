/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view;

/**
 * {@link RuntimeException} for view locations that are not bound.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public class ViewLocationNotBoundException extends RuntimeException {

	private static final long serialVersionUID = -5288144403869126220L;

	/**
	 * Creates an {@link ViewLocationNotBoundException}.
	 */
	public ViewLocationNotBoundException() {
		super();
	}

	/**
	 * Creates an {@link ViewLocationNotBoundException}.
	 * 
	 * @param message
	 *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public ViewLocationNotBoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates an {@link ViewLocationNotBoundException}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
	 */
	public ViewLocationNotBoundException(String message) {
		super(message);
	}

	/**
	 * Creates an {@link ViewLocationNotBoundException}.
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public ViewLocationNotBoundException(Throwable cause) {
		super(cause);
	}

}
