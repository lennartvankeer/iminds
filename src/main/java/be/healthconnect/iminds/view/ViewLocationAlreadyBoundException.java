/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view;

/**
 * {@link RuntimeException} for view locations that are already bound.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public class ViewLocationAlreadyBoundException extends RuntimeException {

	private static final long serialVersionUID = -4780625823897002219L;

	/**
	 * Creates an {@link ViewLocationAlreadyBoundException}.
	 */
	public ViewLocationAlreadyBoundException() {
		super();
	}

	/**
	 * Creates an {@link ViewLocationAlreadyBoundException}.
	 * 
	 * @param message
	 *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public ViewLocationAlreadyBoundException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates an {@link ViewLocationAlreadyBoundException}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
	 */
	public ViewLocationAlreadyBoundException(String message) {
		super(message);
	}

	/**
	 * Creates an {@link ViewLocationAlreadyBoundException}.
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public ViewLocationAlreadyBoundException(Throwable cause) {
		super(cause);
	}

}
