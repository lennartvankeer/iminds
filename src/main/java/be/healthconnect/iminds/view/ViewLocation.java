/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view;

import be.healthconnect.iminds.view.impl.ViewManagerImpl;

/**
 * Enumeration of all available view locations for use with the {@link ViewManagerImpl}. Usually, each view corresponds to a single view
 * location, which represents the location where that view should be embedded.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public enum ViewLocation {

	NAVIGATION,
	
	TABCONTENT;
	
}
