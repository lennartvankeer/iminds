package be.healthconnect.iminds.view;

public interface VitalinkScenarioView extends View{

	void addListener(VitalinkScenarioViewListener vitalinkScenarioViewListener);

	void inform(String message);

}
