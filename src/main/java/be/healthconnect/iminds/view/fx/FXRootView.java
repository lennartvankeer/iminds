package be.healthconnect.iminds.view.fx;

import com.google.inject.Inject;

import be.healthconnect.iminds.IMindsTestToolApp;
import be.healthconnect.iminds.view.RootView;
import be.healthconnect.iminds.view.UnsupportedViewLocationException;
import be.healthconnect.iminds.view.ViewLocation;
import be.healthconnect.iminds.view.ViewManager;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

public class FXRootView extends BorderPane implements RootView{

	
	private final ViewManager viewManager;
	private Scene sc;

	@Inject
	public FXRootView(ViewManager viewManager){
		this.viewManager = viewManager;
		this.getStyleClass().add("rootview");
	}
	
	public void attachChild(Object subView, ViewLocation location) {
		if (!(subView instanceof Node)) {
			throw new IllegalArgumentException("subView must be an instance of " + Node.class.getName());
		}
		switch (location) {
		case NAVIGATION:
			setTop((Node)subView);
			break;
		case TABCONTENT:
			setCenter((Node)subView);
			break;
		default:
			throw new UnsupportedViewLocationException();
		}
	}

	public void detachChild(Object subView) {
		if(getCenter() == subView){
			setCenter(null);
		}else if(getTop() == subView){
			setTop(null);
		}
	}

	public void attach() {
		// The root view is embedded in the main window
		sc = new Scene(this);
		sc.getStylesheets().add("css/stylesheet.css");
		IMindsTestToolApp.getInstance().getStage().setScene(sc);
		
		viewManager.registerLocations(this, ViewLocation.NAVIGATION, ViewLocation.TABCONTENT);
		
	}

	public void detach() {
		throw new UnsupportedOperationException("the rootview may never be detached");
	}

}
