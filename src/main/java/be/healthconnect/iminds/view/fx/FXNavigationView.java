package be.healthconnect.iminds.view.fx;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import be.healthconnect.iminds.dto.Navigation;
import be.healthconnect.iminds.util.I18NUtility;
import be.healthconnect.iminds.view.NavigationView;
import be.healthconnect.iminds.view.NavigationViewListener;
import be.healthconnect.iminds.view.ViewLocation;
import be.healthconnect.iminds.view.ViewManager;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

public class FXNavigationView extends HBox implements NavigationView{

	private final ViewManager viewManager;
	private boolean isInitialized = false;
	private ToggleButton vitalinkScenarioBtn;
	private ToggleButton logBtn;
	private final List<NavigationViewListener> listeners = new ArrayList<>();
	private final ToggleGroup toggleGroup = new ToggleGroup();
	
	@Inject
	public FXNavigationView(ViewManager viewManager){
		this.viewManager = viewManager;	
		getStyleClass().add("navigationBar");
	}
	
	
	public void attachChild(Object subView, ViewLocation location) {
		throw new UnsupportedOperationException("no children can be attached to this view");
	}

	public void detachChild(Object subView) {
		throw new UnsupportedOperationException("no children can be detached to this view");
	}

	public void attach() {
		viewManager.attach(this, ViewLocation.NAVIGATION);
		init();
	}

	private void init() {
		if(! isInitialized){
			getChildren().addAll(getVitalinkScenatioBtn(),getLogBtn());			
			isInitialized = true;
		}
		
	}


	private ToggleButton getVitalinkScenatioBtn() {
		if(vitalinkScenarioBtn == null){
			vitalinkScenarioBtn = new ToggleButton();
			vitalinkScenarioBtn.setOnAction(ev->{
				listeners.forEach(l -> l.navigateTo(Navigation.VITALINKSCENARIO));
			});
			vitalinkScenarioBtn.getStyleClass().add("navigationTab");
			vitalinkScenarioBtn.setText(I18NUtility.getMessage("navigation.tab.vitalinkscenario"));
			vitalinkScenarioBtn.setToggleGroup(toggleGroup);
		}
		return vitalinkScenarioBtn;
	}
	
	private ToggleButton getLogBtn() {
		if(logBtn == null){
			logBtn = new ToggleButton();
			logBtn.setOnAction(ev->{
				listeners.forEach(l -> l.navigateTo(Navigation.LOG));
			});
			logBtn.getStyleClass().add("navigationTab");
			logBtn.setText(I18NUtility.getMessage("navigation.tab.log"));
			logBtn.setToggleGroup(toggleGroup);
		}
		return logBtn;
	}


	public void detach() {
		listeners.clear();
		viewManager.detach(this, ViewLocation.NAVIGATION);
	}


	@Override
	public void setActiveTab(Navigation navigateTo) {
		switch (navigateTo) {
		case VITALINKSCENARIO:
			getVitalinkScenatioBtn().setSelected(true);
			break;
		case LOG:
			getLogBtn().setSelected(true);
			break;
		default:toggleGroup.selectToggle(null);
			break;
		}
	}


	@Override
	public void addListener(NavigationViewListener navigationViewListener) {
		listeners.add(navigationViewListener);
		
	}

}
