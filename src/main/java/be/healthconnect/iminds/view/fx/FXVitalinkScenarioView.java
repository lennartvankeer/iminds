package be.healthconnect.iminds.view.fx;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import be.healthconnect.iminds.util.I18NUtility;
import be.healthconnect.iminds.view.ViewLocation;
import be.healthconnect.iminds.view.ViewManager;
import be.healthconnect.iminds.view.VitalinkScenarioView;
import be.healthconnect.iminds.view.VitalinkScenarioViewListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

public class FXVitalinkScenarioView extends BorderPane implements VitalinkScenarioView{

	private final List<VitalinkScenarioViewListener> listeners = new ArrayList<>();
	private final ViewManager viewManager;
	
	private FileChooser fileChooser;
	private Button fileChooserButton;
	private Label chosenFile;
	private boolean isInitialized = false;
	private File scenarioFile;
	private VBox testFields;
	private TextArea output;
	private HBox buttons;
	private Button clearBtn;
	private Button runBtn;
	
	
	@Inject
	public FXVitalinkScenarioView(ViewManager viewManager) {
		this.viewManager = viewManager;
	}
	
	public void attachChild(Object subView, ViewLocation location) {
		
	}

	public void detachChild(Object subView) {
		
	}

	public void attach() {
		viewManager.attach(this, ViewLocation.TABCONTENT);
		init();
	}

	private void init() {
		if(!isInitialized){
			setTop(getTitle());
			setLeft(getTestFields());
			setCenter(getOutput());
			setBottom(getButtons());
		}
	}	

	private Node getTitle() {		
		Label title = new Label(I18NUtility.getMessage("vitalinkscenario.title"));
		title.getStyleClass().add("testTitle");		
		return title;
	}
	
	private Node getTestFields() {
		if(testFields == null){
			fileChooser = new FileChooser();
			chosenFile = new Label();
			fileChooserButton = new Button(I18NUtility.getMessage("vitalinkscenario.choosefile"));
			fileChooserButton.setOnAction(ev -> {
				scenarioFile = fileChooser.showOpenDialog(getScene().getWindow());
				if(scenarioFile != null){
					chosenFile.setText(scenarioFile.getName());
				}else{
					chosenFile.setText("");
				}
			});
			testFields = new VBox(fileChooserButton, chosenFile);
		}
		return testFields;
	}
	
	private TextArea getOutput() {
		if(output == null){
			output = new TextArea();
		}
		return output;
	}
	
	private Node getButtons() {
		if(buttons == null){
			clearBtn = new Button(I18NUtility.getMessage("vitalinkscenario.clear"));
			clearBtn.setOnAction(ev -> {
				output.clear();
				chosenFile.setText("");
				scenarioFile = null;
			});
			
			runBtn = new Button(I18NUtility.getMessage("vitalinkscenario.run"));
			runBtn.setOnAction(ev -> listeners.forEach(l -> l.runTest(scenarioFile)));
			
			buttons = new HBox(runBtn, clearBtn);
		}
		return buttons;
	}
	
	public void detach() {
		listeners.clear();
		viewManager.detach(this, ViewLocation.TABCONTENT);
		
	}

	@Override
	public void addListener(VitalinkScenarioViewListener vitalinkScenarioViewListener) {
		listeners.add(vitalinkScenarioViewListener);
	}

	@Override
	public void inform(String message) {
		getOutput().appendText(message);
		getOutput().appendText("\n");
	}

}
