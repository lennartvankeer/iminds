package be.healthconnect.iminds.view.fx;

import java.io.Console;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import com.google.inject.Inject;

import be.healthconnect.iminds.util.TextAreaAppender;
import be.healthconnect.iminds.view.LogView;
import be.healthconnect.iminds.view.ViewLocation;
import be.healthconnect.iminds.view.ViewManager;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class FXLogView extends BorderPane implements LogView {

	private final ViewManager viewManager;
	private boolean initialized = false;
	private TextArea console;

	@Inject
	public FXLogView(ViewManager viewManager) {
		this.viewManager = viewManager;
		init();//eager init to get all logging in the view
	}

	@Override
	public void attachChild(Object subView, ViewLocation location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void detachChild(Object subView) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attach() {
		viewManager.attach(this, ViewLocation.TABCONTENT);		
	}

	private void init() {
		if(initialized){
			return;
		}
		setCenter(getConsoleTextArea());		
	}

	private Node getConsoleTextArea() {
		if(console == null){
			console = new TextArea();
			TextAreaAppender.setTextArea(console);			
		}
		return console;
	}

	@Override
	public void detach() {
		viewManager.detach(this, ViewLocation.TABCONTENT);
	}
}
