/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import com.google.inject.Singleton;

import be.healthconnect.iminds.view.View;
import be.healthconnect.iminds.view.ViewLocation;
import be.healthconnect.iminds.view.ViewLocationAlreadyBoundException;
import be.healthconnect.iminds.view.ViewLocationNotBoundException;
import be.healthconnect.iminds.view.ViewManager;


/**
 * Manages the registration of view location points. Specific views can register provided view locations, and they can position themselves
 * at provided view locations.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
@Singleton
public final class ViewManagerImpl implements ViewManager {

	private static final Logger LOG = Logger.getLogger(ViewManagerImpl.class.getName());

	private final Map<ViewLocation, View> locations = new HashMap<ViewLocation, View>();

	/**
	 * {@inheritDoc}
	 */
	public synchronized void registerLocation(final View view, final ViewLocation location) {
		if (locations.containsKey(location)) {
			throw new ViewLocationAlreadyBoundException(location.toString());
		}
		locations.put(location, view);
	}

	/**
	 * {@inheritDoc}
	 */
	public void registerLocations(final View view, final ViewLocation... locations) {
		for (ViewLocation location : locations) {
			registerLocation(view, location);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public synchronized void unregisterLocations(final View view) {
		final Iterator<Entry<ViewLocation, View>> locs = locations.entrySet().iterator();
		while (locs.hasNext()) {
			Entry<ViewLocation, View> location = locs.next();
			if (location.getValue().equals(view)) {
				LOG.fine(String.format("Unregistering view location %s", location.getKey().toString()));
				locs.remove();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void attach(final Object subView, final ViewLocation location) {
		if (!locations.containsKey(location)) {
			throw new ViewLocationNotBoundException(location.toString());
		}
		locations.get(location).attachChild(subView, location);
	}

	/**
	 * {@inheritDoc}
	 */
	public void detach(final Object subView, final ViewLocation location) {
		if (!locations.containsKey(location)) {
			throw new ViewLocationNotBoundException(location.toString());
		}
		locations.get(location).detachChild(subView);
	}
	
}
