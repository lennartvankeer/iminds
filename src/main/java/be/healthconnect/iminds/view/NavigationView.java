package be.healthconnect.iminds.view;

import be.healthconnect.iminds.dto.Navigation;

public interface NavigationView extends View{

	void setActiveTab(Navigation navigateTo);

	void addListener(NavigationViewListener navigationViewListener);

}
