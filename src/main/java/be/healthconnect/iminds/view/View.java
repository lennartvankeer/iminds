/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view;

import be.healthconnect.iminds.view.impl.ViewManagerImpl;

/**
 * Base interface for all views with support for {@link ViewManagerImpl}.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public interface View {

	/**
	 * Attaches <code>subView</code> at <code>location</code>.
	 * 
	 * @param subView
	 *            the sub-view to attach to this view
	 * @param location
	 *            the location where to attach the sub-view
	 */
	void attachChild(Object subView, ViewLocation location);

	/**
	 * Detaches <code>subView</code> from <code>location</code>.
	 * 
	 * @param subView
	 *            the sub-view to detach from this view
	 * @param location
	 *            the location from which to detach the sub-view
	 */
	void detachChild(Object subView);
	
	/**
	 * Attaches this view from its parent view.
	 */
	void attach();
	
	/**
	 * Detaches this view from its parent view.
	 */
	void detach();

}
