package be.healthconnect.iminds.view;


public interface ViewManager {

	/**
	 * Registers <code>location</code> as provided by <code>view</code>.
	 * 
	 * @param view
	 *            the view that provides the given location
	 * @param location
	 *            the provided location
	 */
	void registerLocation(final View view, final ViewLocation location);

	/**
	 * Registers all <code>locations</code> as provided by <code>view</code>.
	 * 
	 * @param view
	 *            the view that provides the given location
	 * @param locations
	 *            the provided locations
	 */
	void registerLocations(final View view, final ViewLocation... locations);

	/**
	 * Unregisters all locations provided by <code>view</code>.
	 * 
	 * @param view
	 *            the view that provides the locations
	 */
	void unregisterLocations(final View view);

	/**
	 * Attaches <code>subView</code> at <code>location</code>.
	 * 
	 * @param subView
	 *            the sub-view to attach to this view
	 * @param location
	 *            the location where to attach the sub-view
	 * @throws ViewLocationNotBoundException
	 *             if <code>location</code> is not registered in this view manager
	 */
	void attach(final Object subView, final ViewLocation location);

	/**
	 * Detaches <code>subView</code> from <code>location</code>.
	 * 
	 * @param subView
	 *            the sub-view to detach from this view
	 * @param location
	 *            the location from which to detach the sub-view
	 * @throws ViewLocationNotBoundException
	 *             if <code>location</code> is not registered in this view manager
	 */
	void detach(final Object subView, final ViewLocation location);

}