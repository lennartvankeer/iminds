/**
 * (C) 2012 HealthConnect CVBA. All rights reserved.
 */
package be.healthconnect.iminds.view;

/**
 * {@link RuntimeException} for unsupported view locations.
 * 
 * @author <a href="mailto:dennis.wagelaar@healthconnect.be">Dennis Wagelaar</a>
 */
public class UnsupportedViewLocationException extends RuntimeException {

	private static final long serialVersionUID = -3085353938650793549L;

	/**
	 * Creates an {@link UnsupportedViewLocationException}.
	 */
	public UnsupportedViewLocationException() {
		super();
	}

	/**
	 * Creates an {@link UnsupportedViewLocationException}.
	 * 
	 * @param message
	 *            the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public UnsupportedViewLocationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates an {@link UnsupportedViewLocationException}.
	 * 
	 * @param message
	 *            the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
	 */
	public UnsupportedViewLocationException(String message) {
		super(message);
	}

	/**
	 * Creates an {@link UnsupportedViewLocationException}.
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
	 *            indicates that the cause is nonexistent or unknown.)
	 */
	public UnsupportedViewLocationException(Throwable cause) {
		super(cause);
	}

}
