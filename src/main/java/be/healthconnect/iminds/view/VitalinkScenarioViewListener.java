package be.healthconnect.iminds.view;

import java.io.File;

public interface VitalinkScenarioViewListener {

	void runTest(File scenarioFile);

}
